package com.gitlabtest.gitlabresttest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabresttestApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabresttestApplication.class, args);
	}

}
